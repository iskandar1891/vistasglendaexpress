$(document).ready(function () {

    $('#iniciar').on("click", function(){
        
        let formdata = new FormData(document.getElementById('login'));

        var objectLogin = {};

        formdata.forEach(function (value, key) {
            objectLogin[key] = value;
            });
            var jsonPaquete = JSON.stringify(objectLogin);

            fetch('http://glenda-express.test/auth/login', {
                    method: 'POST',
                    body: jsonPaquete,
                    async: true,
                })
                .then(function (response) {
                    response.json().then(function (token) {
                        if(response.ok){
                            console.log(token.Token);
                            localStorage.setItem("Token", token.Token);
                            window.location.href="http://vistasglendaexpress.test/pages/clientes/list_clientes.php";
                        } else{
                            console.log(token.messages.error);
                            swal({
                                title: 'Error',
                                text: token.messages.error,
                                type: 'warning',
                                icon: "warning",
                            })
                        }
                      
                    
                    })
                   
                })
    })

})