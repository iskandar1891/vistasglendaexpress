$(document).ready(function () {
    var table

    verClientes();
    updateCliente();
    createCliente();

    ///ocultar y mostrar elemento al cargar pagina
    $('#form-edit-cliente').hide();

    $('#form-new-cliente').hide();

    $('#cerrar-form-cliente').click(function () {
        $('#form-edit-cliente').hide();
        $('#tabla-cliente').show();
    });

    $('#cerrar-form-cliente-new').click(function () {
        $('#form-new-cliente').hide();
        $('#tabla-cliente').show();

        limpiarCampos();
    });



    $('#nuevo-cliente').click(function () {
        $('#form-new-cliente').show();
        $('#tabla-cliente').hide();
    });

    function verClientes() {
        table = $('#zero-config').DataTable({
            "ajax": {
                "url": "http://glenda-express.test/api/clientes",
                "method": 'GET',
                "headers": {
                    "Authorization": "Bearer: " + localStorage.getItem("Token"),
                },

                async: true,
                "dataSrc": "",
                error: function (request, status, error) {
                    window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php";
                }
            },
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",

            "language": {
                "processing": "Procesando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "infoThousands": ",",
                "loadingRecords": "Cargando...",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad",
                    "collection": "Colección",
                    "colvisRestore": "Restaurar visibilidad",
                    "copyKeys": "Presione ctrl o u2318 + C para copiar los datos de la tabla al portapapeles del sistema. <br \/> <br \/> Para cancelar, haga clic en este mensaje o presione escape.",
                    "copySuccess": {
                        "1": "Copiada 1 fila al portapapeles",
                        "_": "Copiadas %d fila al portapapeles"
                    },
                    "copyTitle": "Copiar al portapapeles",
                    "csv": "CSV",
                    "excel": "Excel",
                    "pageLength": {
                        "-1": "Mostrar todas las filas",
                        "1": "Mostrar 1 fila",
                        "_": "Mostrar %d filas"
                    },
                    "pdf": "PDF",
                    "print": "Imprimir"
                },
                "autoFill": {
                    "cancel": "Cancelar",
                    "fill": "Rellene todas las celdas con <i>%d<\/i>",
                    "fillHorizontal": "Rellenar celdas horizontalmente",
                    "fillVertical": "Rellenar celdas verticalmentemente"
                },
                "decimal": ",",
                "searchBuilder": {
                    "add": "Añadir condición",
                    "button": {
                        "0": "Constructor de búsqueda",
                        "_": "Constructor de búsqueda (%d)"
                    },
                    "clearAll": "Borrar todo",
                    "condition": "Condición",
                    "conditions": {
                        "date": {
                            "after": "Despues",
                            "before": "Antes",
                            "between": "Entre",
                            "empty": "Vacío",
                            "equals": "Igual a",
                            "notBetween": "No entre",
                            "notEmpty": "No Vacio",
                            "not": "Diferente de"
                        },
                        "number": {
                            "between": "Entre",
                            "empty": "Vacio",
                            "equals": "Igual a",
                            "gt": "Mayor a",
                            "gte": "Mayor o igual a",
                            "lt": "Menor que",
                            "lte": "Menor o igual que",
                            "notBetween": "No entre",
                            "notEmpty": "No vacío",
                            "not": "Diferente de"
                        },
                        "string": {
                            "contains": "Contiene",
                            "empty": "Vacío",
                            "endsWith": "Termina en",
                            "equals": "Igual a",
                            "notEmpty": "No Vacio",
                            "startsWith": "Empieza con",
                            "not": "Diferente de"
                        },
                        "array": {
                            "not": "Diferente de",
                            "equals": "Igual",
                            "empty": "Vacío",
                            "contains": "Contiene",
                            "notEmpty": "No Vacío",
                            "without": "Sin"
                        }
                    },
                    "data": "Data",
                    "deleteTitle": "Eliminar regla de filtrado",
                    "leftTitle": "Criterios anulados",
                    "logicAnd": "Y",
                    "logicOr": "O",
                    "rightTitle": "Criterios de sangría",
                    "title": {
                        "0": "Constructor de búsqueda",
                        "_": "Constructor de búsqueda (%d)"
                    },
                    "value": "Valor"
                },
                "searchPanes": {
                    "clearMessage": "Borrar todo",
                    "collapse": {
                        "0": "Paneles de búsqueda",
                        "_": "Paneles de búsqueda (%d)"
                    },
                    "count": "{total}",
                    "countFiltered": "{shown} ({total})",
                    "emptyPanes": "Sin paneles de búsqueda",
                    "loadMessage": "Cargando paneles de búsqueda",
                    "title": "Filtros Activos - %d"
                },
                "select": {
                    "1": "%d fila seleccionada",
                    "_": "%d filas seleccionadas",
                    "cells": {
                        "1": "1 celda seleccionada",
                        "_": "$d celdas seleccionadas"
                    },
                    "columns": {
                        "1": "1 columna seleccionada",
                        "_": "%d columnas seleccionadas"
                    }
                },
                "thousands": ".",
                "datetime": {
                    "previous": "Anterior",
                    "next": "Proximo",
                    "hours": "Horas",
                    "minutes": "Minutos",
                    "seconds": "Segundos",
                    "unknown": "-",
                    "amPm": [
                        "am",
                        "pm"
                    ]
                },
                "editor": {
                    "close": "Cerrar",
                    "create": {
                        "button": "Nuevo",
                        "title": "Crear Nuevo Registro",
                        "submit": "Crear"
                    },
                    "edit": {
                        "button": "Editar",
                        "title": "Editar Registro",
                        "submit": "Actualizar"
                    },
                    "remove": {
                        "button": "Eliminar",
                        "title": "Eliminar Registro",
                        "submit": "Eliminar",
                        "confirm": {
                            "_": "¿Está seguro que desea eliminar %d filas?",
                            "1": "¿Está seguro que desea eliminar 1 fila?"
                        }
                    },
                    "error": {
                        "system": "Ha ocurrido un error en el sistema (<a target=\"\\\" rel=\"\\ nofollow\" href=\"\\\">Más información&lt;\\\/a&gt;).<\/a>"
                    },
                    "multi": {
                        "title": "Múltiples Valores",
                        "info": "Los elementos seleccionados contienen diferentes valores para este registro. Para editar y establecer todos los elementos de este registro con el mismo valor, hacer click o tap aquí, de lo contrario conservarán sus valores individuales.",
                        "restore": "Deshacer Cambios",
                        "noMulti": "Este registro puede ser editado individualmente, pero no como parte de un grupo."
                    }
                },
                "info": "Mostrando de _START_ a _END_ de _TOTAL_ entradas"

            },
            "columns": [{
                    "data": "id_cliente"
                },
                {
                    "data": "nombre_cliente"
                },
                {
                    "data": "telefono_cliente"
                },
                {
                    "data": "correo_cliente"
                },
                {
                    "data": "direccion_cliente"
                },
                {
                    "data": "dui_cliente"
                },
                {
                    "defaultContent": "<div class='text-center'>" +
                        "<div class='btn-group'>" +
                        "<button type='button' class='ver btn btn-outline-primary btn-sm'>" +
                        "<svg xmlns='http://www.w3.org/2000/svg' class='h-6 w-6' fill='none' viewBox='0 0 24 24' stroke='currentColor'>" +
                        "<path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z' />" +
                        "</svg>" +
                        "</button>" +
                        "<button class='borrar btn btn-outline-danger btn-sm'>" +
                        "<svg xmlns='http://www.w3.org/2000/svg' class='h-6 w-6' fill='none' viewBox='0 0 24 24' stroke='currentColor'>" +
                        "<path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16' />" +
                        "</svg>" +
                        "</button>" +
                        "</div>" +
                        "</div>"
                }
            ],
            "stripeClasses": [],
            "lengthMenu": [5, 10, 20, 50],
            "pageLength": 5

        });

    }

    ///inicio funcion para ver clientes
    var Ver = function (tbody, table) {
        $(tbody).on("click", "button.ver", function () {

            $('#form-edit-cliente').show();
            $('#tabla-cliente').hide();

            var data = table.row($(this).parents("tr")).data();
            $("#id_cliente").val(data.id_cliente);
            $("#nombre_cliente").val(data.nombre_cliente);
            $("#telefono_cliente").val(data.telefono_cliente);
            $("#correo_cliente").val(data.correo_cliente);
            $("#direccion_cliente").val(data.direccion_cliente);
        });
    }
    Ver("#zero-config tbody", table);

    function updateCliente() {
        $('#enviar').on("click", function () {
            var datos = new FormData(document.getElementById('formulario'));
            var object = {};
            datos.forEach(function (value, key) {
                object[key] = value;
            });
            var json = JSON.stringify(object);
            id_cliente = $("#id_cliente").val();
            fetch('http://glenda-express.test/api/clientes/' + id_cliente, {
                    method: 'PUT',
                    body: json,
                    "headers": {
                        "Authorization": "Bearer: " + localStorage.getItem("Token"),
                    },
                    async: true,
                })
                .then(function (response) {
                    if (response.ok) {
                        swal({
                            title: 'Exito',
                            text: 'Cliente editado con exito',
                            type: 'success',
                        })
                        response => response.json()
                        $('#form-edit-cliente').hide();
                        $('#tabla-cliente').show();
                        $('#zero-config').DataTable().ajax.reload();
                    } else {
                        throw "Error en la llamada Ajax";
                    }
                })
                .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
        })
    }

    function createCliente() {
        $('#crear').on("click", function () {
            var newCliente = new FormData(document.getElementById('formulario-created'));
            if ($('#nombre_cliente2').val() == '' | $('#dui_cliente').val() == '' | $('#telefono_cliente2').val() == '' | $('#correo_cliente2').val() == '' | $('#direccion_cliente2').val() == '') {
                swal("Error!", "Rellena los campos vacios", "error");
            } else {
                var objectNewCliente = {};
                newCliente.forEach(function (value, key) {
                    objectNewCliente[key] = value;
                });
                var jsonCliente = JSON.stringify(objectNewCliente);

                fetch('http://glenda-express.test/api/clientes', {
                        method: 'POST',
                        body: jsonCliente,
                        "headers": {
                            "Authorization": "Bearer: " + localStorage.getItem("Token"),
                        },
                        async: true,
                    })
                    .then(function (response) {
                        if (response.ok) {
                            swal({
                                title: 'Exito',
                                text: 'Cliente Creado con exito',
                                type: 'success',
                            })

                            response.json().then(function (cliente) {
                                console.log(cliente.id_cliente);
                            })

                            $('#form-edit-cliente').hide();
                            $('#form-new-cliente').hide();
                            $('#tabla-cliente').show();
                            $('#zero-config').DataTable().ajax.reload();

                            limpiarCampos();
                        }
                    })
                    .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
            }
        })
    }

    ///inicio funcion para elimiar docente
    var Eliminar = function (tbody, table) {

        $(tbody).on("click", "button.borrar", function () {
            var cliente = table.row($(this).parents("tr")).data();

            swal({
                    title: "¿Eliminar cliente?",
                    text: "Esta accion no tiene retorno",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        fetch('http://glenda-express.test/api/clientes/' + cliente.id_cliente, {
                                method: 'DELETE',
                                "headers": {
                                    "Authorization": "Bearer: " + localStorage.getItem("Token"),
                                },
                                async: true,
                                // body: json
                            })
                            .then(function (response) {
                                if (response.ok) {
                                    swal({
                                        title: 'Exito',
                                        text: 'Cliente eliminado con exito',
                                        type: 'success',
                                    })
                                    response => response.json()
                                    $('#zero-config').DataTable().ajax.reload();
                                } else {
                                    throw "Error en la llamada Ajax";
                                }
                            })
                            .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
                    }
                });
        });
    }
    Eliminar("#zero-config tbody", table);
    ///fin funcion para elimiar docente

    function limpiarCampos() {
        $("#id_cliente2").val("");
        $("#nombre_cliente2").val("");
        $("#telefono_cliente2").val("");
        $("#correo_cliente2").val("");
        $("#direccion_cliente2").val("");
        $("#dui_cliente").val("");
    }

    function validaVencimiento() {

    }


});