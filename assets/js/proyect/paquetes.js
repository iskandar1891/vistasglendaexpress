$(document).ready(function () {

    var table, detalle, title, nuevoProducto, cliente, table2;
    var eliminar = {}

    verPaquetes();
    selectClientes();
    newProduct();

    crearPaquete();

    updateEstadoPaquete();


    $('#form-view-paquete').hide();
    $('#form-new-paquete').hide();
    $('#form-update-llegada').hide();
   


    $('#cerrar-view-detalle-paquete').click(function () {
        $('#formulario').empty();
        $('#title').empty();
        title = " ";
        $('#form-view-paquete').hide();
        $('#tabla-paquete').show(); 

    });

    $('#cerrar-view-crear-paquete').click(function () {
        $('#form-new-paquete').hide();
        $('#tabla-paquete').show();
    });

    $('#cerrar-estado').click(function () {
        $('#tabla-paquete').show();
        $('#form-update-llegada').hide();
    });

    


    $('#nuevo-paquete').click(function () {
        $('#form-new-paquete').show();
        $('#tabla-paquete').hide();
    });

    function verPaquetes() {
        table = $('#zero-config').DataTable({
            "ajax": {
                "url": "http://glenda-express.test/api/paquetes",
                "method": 'GET',
                "headers":{
                    "Authorization": "Bearer: " + localStorage.getItem("Token"),
                },
                async: true,
                "dataSrc": "",
                error :function(request, status, error) {
                    window.location.href="http://vistasglendaexpress.test/pages/usuarios/login.php";
                }
            },
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",

            "language": {
                "processing": "Procesando...",
                "lengthMenu": "Mostrar MENU registros",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de MAX registros)",
                "search": "Buscar:",
                "infoThousands": ",",
                "loadingRecords": "Cargando...",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad",
                    "collection": "Colección",
                    "colvisRestore": "Restaurar visibilidad",
                    "copyKeys": "Presione ctrl o u2318 + C para copiar los datos de la tabla al portapapeles del sistema. <br \/> <br \/> Para cancelar, haga clic en este mensaje o presione escape.",
                    "copySuccess": {
                        "1": "Copiada 1 fila al portapapeles",
                        "_": "Copiadas %d fila al portapapeles"
                    },
                    "copyTitle": "Copiar al portapapeles",
                    "csv": "CSV",
                    "excel": "Excel",
                    "pageLength": {
                        "-1": "Mostrar todas las filas",
                        "1": "Mostrar 1 fila",
                        "_": "Mostrar %d filas"
                    },
                    "pdf": "PDF",
                    "print": "Imprimir"
                },
                "autoFill": {
                    "cancel": "Cancelar",
                    "fill": "Rellene todas las celdas con <i>%d<\/i>",
                    "fillHorizontal": "Rellenar celdas horizontalmente",
                    "fillVertical": "Rellenar celdas verticalmentemente"
                },
                "decimal": ",",
                "searchBuilder": {
                    "add": "Añadir condición",
                    "button": {
                        "0": "Constructor de búsqueda",
                        "_": "Constructor de búsqueda (%d)"
                    },
                    "clearAll": "Borrar todo",
                    "condition": "Condición",
                    "conditions": {
                        "date": {
                            "after": "Despues",
                            "before": "Antes",
                            "between": "Entre",
                            "empty": "Vacío",
                            "equals": "Igual a",
                            "notBetween": "No entre",
                            "notEmpty": "No Vacio",
                            "not": "Diferente de"
                        },
                        "number": {
                            "between": "Entre",
                            "empty": "Vacio",
                            "equals": "Igual a",
                            "gt": "Mayor a",
                            "gte": "Mayor o igual a",
                            "lt": "Menor que",
                            "lte": "Menor o igual que",
                            "notBetween": "No entre",
                            "notEmpty": "No vacío",
                            "not": "Diferente de"
                        },
                        "string": {
                            "contains": "Contiene",
                            "empty": "Vacío",
                            "endsWith": "Termina en",
                            "equals": "Igual a",
                            "notEmpty": "No Vacio",
                            "startsWith": "Empieza con",
                            "not": "Diferente de"
                        },
                        "array": {
                            "not": "Diferente de",
                            "equals": "Igual",
                            "empty": "Vacío",
                            "contains": "Contiene",
                            "notEmpty": "No Vacío",
                            "without": "Sin"
                        }
                    },
                    "data": "Data",
                    "deleteTitle": "Eliminar regla de filtrado",
                    "leftTitle": "Criterios anulados",
                    "logicAnd": "Y",
                    "logicOr": "O",
                    "rightTitle": "Criterios de sangría",
                    "title": {
                        "0": "Constructor de búsqueda",
                        "_": "Constructor de búsqueda (%d)"
                    },
                    "value": "Valor"
                },
                "searchPanes": {
                    "clearMessage": "Borrar todo",
                    "collapse": {
                        "0": "Paneles de búsqueda",
                        "_": "Paneles de búsqueda (%d)"
                    },
                    "count": "{total}",
                    "countFiltered": "{shown} ({total})",
                    "emptyPanes": "Sin paneles de búsqueda",
                    "loadMessage": "Cargando paneles de búsqueda",
                    "title": "Filtros Activos - %d"
                },
                "select": {
                    "1": "%d fila seleccionada",
                    "_": "%d filas seleccionadas",
                    "cells": {
                        "1": "1 celda seleccionada",
                        "_": "$d celdas seleccionadas"
                    },
                    "columns": {
                        "1": "1 columna seleccionada",
                        "_": "%d columnas seleccionadas"
                    }
                },
                "thousands": ".",
                "datetime": {
                    "previous": "Anterior",
                    "next": "Proximo",
                    "hours": "Horas",
                    "minutes": "Minutos",
                    "seconds": "Segundos",
                    "unknown": "-",
                    "amPm": [
                        "am",
                        "pm"
                    ]
                },
                "editor": {
                    "close": "Cerrar",
                    "create": {
                        "button": "Nuevo",
                        "title": "Crear Nuevo Registro",
                        "submit": "Crear"
                    },
                    "edit": {
                        "button": "Editar",
                        "title": "Editar Registro",
                        "submit": "Actualizar"
                    },
                    "remove": {
                        "button": "Eliminar",
                        "title": "Eliminar Registro",
                        "submit": "Eliminar",
                        "confirm": {
                            "_": "¿Está seguro que desea eliminar %d filas?",
                            "1": "¿Está seguro que desea eliminar 1 fila?"
                        }
                    },
                    "error": {
                        "system": "Ha ocurrido un error en el sistema (<a target=\"\\\" rel=\"\\ nofollow\" href=\"\\\">Más información&lt;\\\/a&gt;).<\/a>"
                    },
                    "multi": {
                        "title": "Múltiples Valores",
                        "info": "Los elementos seleccionados contienen diferentes valores para este registro. Para editar y establecer todos los elementos de este registro con el mismo valor, hacer click o tap aquí, de lo contrario conservarán sus valores individuales.",
                        "restore": "Deshacer Cambios",
                        "noMulti": "Este registro puede ser editado individualmente, pero no como parte de un grupo."
                    }
                },
                "info": "Mostrando de START a END de TOTAL entradas"

            },
            "columns": [{
                    "data": "id_paquete"
                },
                {
                    "data": "descripcion_paquete"
                },
                {
                    "data": "cantidad_articulos"
                },
                {
                    "data": "total_peso"
                },
                {
                    "data": "numero_traking"
                },
                {
                    "data": "nombre_cliente"
                },
                {
                    "data": "telefono_cliente"
                },
                {
                    "data": "direccion_cliente"
                },
                {
                    "data": "estado_entrega"
                },
                {
                    "defaultContent": "<div class='text-center'>" +
                        "<div class='btn-group'>" +
                        "<button type='button' class='historial btn btn-outline-primary btn-sm'>" +
                        "<svg xmlns='http://www.w3.org/2000/svg' class='h-6 w-6' fill='none' viewBox='0 0 24 24' stroke='currentColor'><path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z' /></svg>"+
                        "</button>" +
                        "<button type='button' class='editar btn btn-outline-primary btn-sm'>" +
                        "<svg xmlns='http://www.w3.org/2000/svg' class='h-6 w-6' fill='none' viewBox='0 0 24 24' stroke='currentColor'><path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z' /></svg>"+
                        "</button>" +
                        "<button type='button' class='ver btn btn-outline-primary btn-sm'>" +
                        "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-eye'><path d='M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z'></path><circle cx='12' cy='12' r='3'></circle></svg>" +
                        "</button>" +
                        "<button class='borrar btn btn-outline-danger btn-sm'>" +
                        "<svg xmlns='http://www.w3.org/2000/svg' class='h-6 w-6' fill='none' viewBox='0 0 24 24' stroke='currentColor'>" +
                        "<path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16' />" +
                        "</svg>" +
                        "</button>" +
                        "</div>" +
                        "</div>"
                }
            ],
            "stripeClasses": [],
            "lengthMenu": [5, 10, 20, 50],
            "pageLength": 5

        });

    }
    ///inicio funcion para ver clientes
    var Ver = function (tbody, table) {
        $(tbody).on("click", "button.ver", function () {

            //$('#form-edit-cliente').show();
            //$('#tabla-cliente').hide();
            var data = table.row($(this).parents("tr")).data();

            fetch('http://glenda-express.test/api/detalles-paquetes/ver/' + data.id_paquete, {
                    method: 'GET',
                    "headers":{
                        "Authorization": "Bearer: " + localStorage.getItem("Token"),
                    },
                    async: true,
                })
                .then(function (response) {
                    //mostrar form para ver el detalle del paquete
                    $('#form-view-paquete').show();
                    $('#tabla-paquete').hide();

                    response.json().then(function (data) {
                        title = "<b> Detalle de Paquete: " + data[0].descripcion_paquete + "</b>";
                        $('#title').append(title);
                        $.each(data, function (i, data) {


                            detalle = "<hr><div class='form-row mb-4'>" +
                                "<div class='form-group col-md-6' hidden>" +
                                "<label for='inputPassword4'>ID</label>" +
                                "<input type='text' class='form-control' name='id_detalle'" +
                                "id='id_detalle' placeholder='id'>" + data.id_detalle +
                                "</div>" +
                                "<div class='form-group col-md-6'>" +
                                "<label for='inputEmail4'>Producto</label>" +
                                "<label class='form-control' name='producto' id='producto'><b>" + data.producto + "</b>" +
                                "</div>" +
                                "<div class='form-group col-md-6'>" +
                                "<label for='inputPassword4'>Cantidad</label>" +
                                "<label class='form-control' name='cantidad' id='cantidad'><b>" + data.cantidad + "</b>" +
                                "</div>" +
                                "<div class='form-group col-md-6'>" +
                                "<label for='inputEmail4'>Precio</label>" +
                                "<label class='form-control' name='precio'><b>$ " + data.precio + "</b>" +
                                "</div>" +
                                "<div class='form-group col-md-6'>" +
                                "<label for='inputPassword4'>Peso</label>" +
                                "<label class='form-control' name='peso' id='peso'><b> " + data.peso + " LBS" + "</b>" +
                                "</div>" +
                                "<div class='form-group col-md-6'>" +
                                "<label for='inputPassword4'>Impuesto</label>" +
                                "<label class='form-control' name='impuesto' id='impuesto'><b>$ " + data.impuesto + "</b>" +
                                "</div>" +
                                "</div>";

                            $('#formulario').append(detalle);
                        })
                    })
                })
                .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
        });
    }
    Ver("#zero-config tbody", table);


     ///inicio funcion para ver clientes
     var editar = function (tbody, table) {
        $(tbody).on("click", "button.editar", function () {

            $('#form-update-llegada').show();
            $('#tabla-paquete').hide();

            var date = table.row($(this).parents("tr")).data();
            $("#id_paquete").val(date.id_paquete);
            $("#descripcion_paquete2").text(date.descripcion_paquete);
            $("#numero_traking2").text(date.numero_traking);
            $("#estado_entrega").val(date.estado_entrega);
        });
    }
    editar("#zero-config tbody", table);

    function updateEstadoPaquete() {

        $('#actualizar_estado').on("click", function () {

            let id = $("#id_paquete").val();
            let estado_entrega=  $("#estado_entrega").val();

        
            jsonUpdateEstado = {
                "estado_entrega": estado_entrega
            }
            var jsonState = JSON.stringify(jsonUpdateEstado);

            console.log(jsonState);
            
            //console.log('http://glenda-express.test/api/paquetes/' +id );
            
           fetch('http://glenda-express.test/api/paquetes/' + id, {
                    method: 'PUT',
                    body: jsonState,
                    "headers":{
                        "Authorization": "Bearer: " + localStorage.getItem("Token"),
                    },
                    async: true,
                })
                .then(function (response) {
                    if (response.ok) {
                        $('#zero-config').DataTable().ajax.reload();
                        swal({
                            title: 'Exito',
                            text: 'Cliente Creado con exito',
                            type: 'success',
                        })


                        response.json().then(function (cliente) {
                            $('#form-update-llegada').hide();
                            $('#tabla-paquete').show();
                            llegadas = {
                                "id_paquete": id,
                                "estado_llegada": estado_entrega
                            }
                            var jsonLlegada = JSON.stringify(llegadas);
                            fetch('http://glenda-express.test/api/llegadas', {
                                method: 'POST',
                                body: jsonLlegada,
                                "headers":{
                                    "Authorization": "Bearer: " + localStorage.getItem("Token"),
                                },
                                async: true,
                            })
                            .then(function (response) {
                                console.log("llegada creada con exxito");
                            })
                          
                   
                        })
                    }
                })
                //.catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");*/

        })
        
    }




    //listar clientes
    function selectClientes() {
        fetch('http://glenda-express.test/api/clientes', {
                method: 'GET',
                "headers":{
                    "Authorization": "Bearer: " + localStorage.getItem("Token"),
                },
                async: true,
            })
            .then(function (response) {
                response.json().then(function (data) {
                    $.each(data, function (i, data) {
                        cliente = "<option value=" + data.id_cliente + ">" + data.nombre_cliente + "</option>";
                        $('#id_cliente').append(cliente);
                    })
                })
            })
            .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
    }

    //funcion que genera un formulario al cargar la pagina en la etiqueta form id detalle
    function newProduct() {
        nuevoProducto = "<div class='producto form-row mb-4'>" +
            "<div class='form-group col-md-12'>" +
            "<label for='inputEmail4'>Producto</label>" +
            "<input type='text' class='form-control' name='producto'" +
            "id='producto' placeholder='Ingrese nombre de producto'" +
            "required>" +
            "</div>" +
            "<div class='form-group col-md-6'>" +
            "<label for='inputEmail4'>Cantidad</label>" +
            "<input type='number' class='form-control' name='cantidad'" +
            "id='cantidad' placeholder='Cantidad' required>" +
            "</div>" +
            "<div class='form-group col-md-6'>" +
            "<label for='inputEmail4'>Precio</label>" +
            "<input type='number' class='form-control' name='precio'" +
            "id='precio' placeholder='precio' required>" +
            "</div>" +
            "<div class='form-group col-md-6'>" +
            "<label for='inputEmail4'>Peso LBS</label>" +
            "<input type='number' class='form-control' name='peso' id='peso'" +
            "placeholder='peso' required>" +
            "</div>" +
            "<div class='form-group col-md-6'>" +
            "<label for='inputEmail4'>Impuesto</label>" +
            "<input type='number' class='form-control' name='impuesto'" +
            "id='impuesto' placeholder='Impuesto' required>" +
            "</div>" +
            "</div><hr>"
        $('#detalle').append(nuevoProducto);
    }

    //evento click en el boton crear-producto para que inserte un nuevo formulario
    $('#crear-producto').click(function () {
        newProduct();
    });

    //crear paquete 
    function crearPaquete() {
        $('#crear-paquete').on("click", function () {

            ///se crea una variable form data y se catura todo lo que esta en el formulario html detalle sy se convierte en un objeto
            var formdata = $("#detalle").serializeArray();

            //console.log(formdata);

            //se decalara el arreglo new detallle
            var objectNewDetalle = {};

            let array = [];

            let cont = 0;
            let sumCant = 0;
            let sumPrecio = 0;
            let sumPeso = 0;
            let sumImpuesto = 0;

            ///con este foreach se recorre el form data creado del form html detalle cada iteracion
            ///suma uno a la variable cont y se agrega una posicion con su llave valor al arreglo objectNewDetalle
            //luego se ejecuta la condicion que cuando el cont llegue a 5 su valor se resete a cero y agregue el arreglo objectnewdetalle 
            // al objeto array
            formdata.forEach(function (value, key) {
                cont++;
                objectNewDetalle = {
                    ...objectNewDetalle,
                    [value.name]: value.value
                };
                if (cont === 5) {

                    cantidadPaquete = parseFloat(objectNewDetalle.cantidad);
                    precioPaquete = parseFloat(objectNewDetalle.precio);
                    pesoPaquete = parseFloat(objectNewDetalle.peso);
                    impuestoPaquete = parseFloat(objectNewDetalle.impuesto);

                    sumCant += cantidadPaquete;
                    sumPrecio += precioPaquete;
                    sumPeso += pesoPaquete;
                    sumImpuesto += impuestoPaquete;

                    cont = 0
                    array.push(objectNewDetalle)
                }
            });
            //console.log(array);



            let newPaquete = new FormData(document.getElementById('paquete-cabecera'));
            ///se agregan las sumatorias al arreglo de paquetes
            newPaquete.append("total_paquete", sumPrecio);
            newPaquete.append("cantidad_articulos", sumCant);
            newPaquete.append("total_impuesto", sumImpuesto);
            newPaquete.append("total_peso", sumPeso);

            var objectNewPaquete = {};

            newPaquete.forEach(function (value, key) {
                objectNewPaquete[key] = value;
            });
            var jsonPaquete = JSON.stringify(objectNewPaquete);

            //console.log(jsonPaquete);

            fetch('http://glenda-express.test/api/paquetes', {
                    method: 'POST',
                    body: jsonPaquete,
                    "headers":{
                        "Authorization": "Bearer: " + localStorage.getItem("Token"),
                    },
                    async: true,
                })
                .then(function (response) {
                    if (response.ok) {
                        response.json().then(function (paquete) {

                            for (let i = 0; i < array.length; i++) {
                                array[i].id_paquete = paquete.id_paquete;
                            }
                            var jsonDETALLE = JSON.stringify(array);
                            

                            fetch('http://glenda-express.test/api/detallepaquete', {
                                    method: 'POST',
                                    body: jsonDETALLE,
                                    "headers":{
                                        "Authorization": "Bearer: " + localStorage.getItem("Token"),
                                    },
                                    async: true,
                                })
                                .then(function (response) {
                                    if (response.ok) {
                                        swal({
                                            title: 'Exito',
                                            text: 'Paquete Creado con exito',
                                            type: 'success',
                                        })
                                        
                                        $('#form-new-paquete').hide();
                                        $('#tabla-paquete').show();
                                        $('#detalle').empty();
                                        newProduct();
                                        limpiarCampos();
                                        llegadaDefault(paquete.id_paquete)
                                    }
                                })


                        })

                        $('#zero-config').DataTable().ajax.reload();
                    }
                })
                .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
        })
    }

    var CambiarEstado = function (tbody, table) {

        $(tbody).on("click", "button.borrar", function () {
            let paquete = table.row($(this).parents("tr")).data();

            eliminar = {
                "estado": "Eliminado"
            }
            var json = JSON.stringify(eliminar);
            //console.log(json);


            swal({
                    title: "¿Eliminar Paquete?",
                    text: "Esta accion no tiene retorno",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        fetch('http://glenda-express.test/api/paquetes/' + paquete.id_paquete, {
                                method: 'PUT',
                                body: json,
                                "headers":{
                                    "Authorization": "Bearer: " + localStorage.getItem("Token"),
                                },
                                async: true,
                            })
                            .then(function (response) {
                                if (response.ok) {
                                    swal({
                                        title: 'Exito',
                                        text: 'Paquete eliminado con exito',
                                        type: 'success',
                                    })
                                    response => response.json()
                                    $('#zero-config').DataTable().ajax.reload();
                                } else {
                                    throw "Error en la llamada Ajax";
                                }
                            })
                            .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
                    }
                })
                .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
        });
    }
    CambiarEstado("#zero-config tbody", table);

    function limpiarCampos(){
        $("#descripcion_paquete").val("");
        $("#numero_traking").val("");
        $("#id_cliente").val("Seleccione");
          
    }

    function llegadaDefault($id){
        let fecha = new Date('2021-12-05').toISOString().slice(0, 19).replace('T', ' ')
        llegada = {
            "id_paquete": $id,
            "fecha": fecha
        }

        var llegadaProduct = JSON.stringify(llegada);

        fetch('http://glenda-express.test/api/llegadas', {
                    method: 'POST',
                    body: llegadaProduct,
                    "headers":{
                        "Authorization": "Bearer: " + localStorage.getItem("Token"),
                    },
                    async: true,
                })
                .then(function (response) {
                    if (response.ok) {
                        response => response.json()
                        //$('#zero-config').DataTable().ajax.reload();
                    } else {
                        throw "Error en la llamada Ajax";
                    }
                })
                .catch( error=>window.location.href = "http://vistasglendaexpress.test/pages/usuarios/login.php");
    }

  

})