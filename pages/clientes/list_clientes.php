<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>CORK Admin Template - Striped DataTables</title>
    <link rel="icon" type="image/x-icon" href="../../assets/img/favicon.ico" />
    <link href="../../assets/css/loader.css" rel="stylesheet" type="text/css" />
    <script src="../../assets/js/loader.js"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="../../plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="../../plugins/table/datatable/dt-global_style.css">
    <link href="../../plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css">

    <!-- END PAGE LEVEL STYLES -->
</head>

<body class="sidebar-noneoverflow">

    <!-- BEGIN LOADER -->
    <div id="load_screen">
        <div class="loader">
            <div class="loader-content">
                <div class="spinner-grow align-self-center"></div>
            </div>
        </div>
    </div>
    <!--  END LOADER -->

    <!--  BEGIN NAVBAR  -->
    <?php
        include '../menu.php';
    ?>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="page-header">
                    <nav class="breadcrumb-one" aria-label="breadcrumb">
                        <div class="title">
                            <h3>Clientes</h3>
                        </div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"><a href="javascript:void(0);">Lista de Clientes</a></li>
                        </ol>
                    </nav>
                </div>


                <div class="row layout-top-spacing" id="tabla-cliente">

                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing" id="tabla-cliente">
                        <div class="widget-content widget-content-area br-6">
                            <div>
                            <li style="list-style: none;">
                                <a href="javascript:void(0)" class="tabmenu btn btn-primary text-white mt-3 mr-2 float-right" id="nuevo-cliente">Nuevo Cliente</a>
                            </li>
                            </div>
                            <table id="zero-config" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Direccion</th>
                                        <th>DUI</th>
                                        <th class="no-content">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Correo</th>
                                        <th>Direccion</th>
                                        <th>DUI</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row layout-top-spacing" id="form-edit-cliente">
                    <div class="container">
                        <div class="container">
                            <div class="row">
                                <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>Actualizar cliente</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">
                                            <form class="m-4" id="formulario">
                                                <div class="form-row mb-4">
                                                    <div class="form-group col-md-6" hidden>
                                                        <label for="inputPassword4">ID</label>
                                                        <input type="text" class="form-control" name="id_cliente"
                                                            id="id_cliente" placeholder="id">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputEmail4">Nombre</label>
                                                        <input type="text" class="form-control" name="nombre_cliente"
                                                            id="nombre_cliente" placeholder="nombre" required>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Telefono</label>
                                                        <input type="text" class="form-control" name="telefono_cliente"
                                                            id="telefono_cliente" placeholder="Telefono" required>
                                                    </div>
                                                </div>
                                                <div class="form-row mb-4">
                                                    <div class="form-group col-md-6">
                                                        <label for="inputEmail4">Correo</label>
                                                        <input type="email" class="form-control" name="correo_cliente"
                                                            id="correo_cliente" placeholder="correo" required>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Direccion</label>
                                                        <input type="text" class="form-control" name="direccion_cliente"
                                                            id="direccion_cliente" placeholder="direccion" required>
                                                    </div>
                                                </div>
                                                <a href="javascript:void(0)" id="enviar"
                                                    class="btn btn-outline-success mt-2">Actualizar</a>
                                                <a href="javascript:void(0)" id="cerrar-form-cliente"
                                                    class="btn btn-outline-danger mt-2">Cerrar</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row layout-top-spacing" id="form-new-cliente">
                    <div class="container">
                        <div class="container">
                            <div class="row">
                                <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4>Crear Cliente</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">
                                            <form class="m-4" id="formulario-created">
                                                <div class="form-row mb-4">
                                                    <div class="form-group col-md-4">
                                                        <label for="inputEmail4">Nombre</label>
                                                        <input type="text" class="form-control" name="nombre_cliente"
                                                            id="nombre_cliente2" placeholder="nombre" value="cesar" required>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="inputPassword4">DUI</label>
                                                        <input type="text" class="form-control" name="dui_cliente"
                                                            id="dui_cliente" placeholder="Telefono" value="044747474" required>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="inputPassword4">Telefono</label>
                                                        <input type="text" class="form-control" name="telefono_cliente"
                                                            id="telefono_cliente2" placeholder="Telefono" value="7585874" required>
                                                    </div>
                                                </div>
                                                <div class="form-row mb-4">
                                                    <div class="form-group col-md-6">
                                                        <label for="inputEmail4">Correo</label>
                                                        <input type="email" class="form-control" name="correo_cliente"
                                                            id="correo_cliente2" placeholder="correo" value="m@gmail.com" required>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Direccion</label>
                                                        <input type="text" class="form-control" name="direccion_cliente"
                                                            id="direccion_cliente2" placeholder="direccion" value="san miguel" required>
                                                    </div>
                                                </div>
                                                <a href="javascript:void(0)" id="crear"
                                                    class="btn btn-outline-success mt-2">Guardar</a>
                                                <a href="javascript:void(0)" id="cerrar-form-cliente-new"
                                                    class="btn btn-outline-danger mt-2">Cerrar</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">Copyright © 2021 <a target="_blank" href="https://designreset.com">DesignReset</a>, All
                        rights reserved.</p>
                </div>
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart">
                            <path
                                d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                            </path>
                        </svg></p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="../../assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/js/app.js"></script>
    <script src="../../assets/js/proyect/clientes.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    <script>
        $(document).ready(function () {
            App.init();
        });
    </script>
    <script src="../../assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../../plugins/table/datatable/datatables.js"></script>
    <script>

    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>