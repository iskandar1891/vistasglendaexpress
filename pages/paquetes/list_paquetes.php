<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>CORK Admin Template - Striped DataTables</title>
    <link rel="icon" type="image/x-icon" href="../../assets/img/favicon.ico" />
    <link href="../../assets/css/loader.css" rel="stylesheet" type="text/css" />
    <script src="../../assets/js/loader.js"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="../../plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="../../plugins/table/datatable/dt-global_style.css">
    <link href="../../plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css">

    <!-- END PAGE LEVEL STYLES -->
</head>

<body class="sidebar-noneoverflow">

    <!-- BEGIN LOADER -->
    <div id="load_screen">
        <div class="loader">
            <div class="loader-content">
                <div class="spinner-grow align-self-center"></div>
            </div>
        </div>
    </div>
    <!--  END LOADER -->

    <!--  BEGIN NAVBAR  -->
    <?php
        include '../menu.php';
    ?>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="page-header">
                    <nav class="breadcrumb-one" aria-label="breadcrumb">
                        <div class="title">
                            <h3>Paquetes</h3>
                        </div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"><a href="javascript:void(0);">Lista de Paquetes</a></li>
                        </ol>
                    </nav>
                </div>


                <div class="row layout-top-spacing" id="tabla-paquete">

                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing" id="tabla-paquete">
                        <div class="widget-content widget-content-area br-6">
                            <div>
                                <li style="list-style: none;">
                                    <a href="javascript:void(0)"
                                        class="tabmenu btn btn-primary text-white mt-3 mr-2 float-right"
                                        id="nuevo-paquete">Crear Paquete</a>
                                </li>
                            </div>
                            <table id="zero-config" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Descripcion</th>
                                        <th>Articulos</th>
                                        <th>Peso LBS</th>
                                        <th>Traking</th>
                                        <th>Cliente</th>
                                        <th>Telefono</th>
                                        <th>Direccion</th>
                                        <th class="text-center">Estado</th>
                                        <th class="no-content">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Descripcion</th>
                                        <th>Articulos</th>
                                        <th>Peso LBS</th>
                                        <th>Traking</th>
                                        <th>Cliente</th>
                                        <th>Telefono</th>
                                        <th>Direccion</th>
                                        <th class="text-center">Estado</th>
                                        <th class="no-content">Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row layout-top-spacing" id="form-view-paquete">
                    <div class="container">
                        <div class="container">
                            <div class="row">
                                <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">

                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4 id="title"> </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">
                                            <form class="m-4" id="formulario">
                                            </form>
                                            <a href="javascript:void(0)" id="cerrar-view-detalle-paquete"
                                                class="btn btn-outline-danger ml-4 mb-4">Cerrar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row layout-top-spacing" id="form-new-paquete">
                    <div class="container">
                        <div class="container">
                            <div class="row">
                                <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">

                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4 id="title">Crear Paquete </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">


                                            <form class="m-4" id="paquete-cabecera">
                                                <div class='form-row mb-4'>
                                                    <div class='form-group col-md-6'>
                                                        <label>Descripcion del paquete</label><br>
                                                        <input type="text" class="form-control"
                                                            name="descripcion_paquete" id="descripcion_paquete"
                                                            placeholder="Nombre paquete" required>
                                                        <hr>
                                                    </div>
                                                    <div class='form-group col-md-6'>
                                                        <label>Numero de Traking</label><br>
                                                        <input type="text" class="form-control" name="numero_traking"
                                                            id="numero_traking" placeholder="Nombre paquete" required>
                                                        <hr>
                                                    </div>
                                                    <div class='form-group col-md-6'>
                                                        <label>Cliente</label><br>
                                                        <select class="form-control" name="id_cliente" id="id_cliente">
                                                            <option selected disabled>Seleccione</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </form>
                                            <h4 class='col-sm-12'>Detalle de paquete</h4><br>
                                            <form class="m-4" id="detalle">
                                                

                                            </form>
                                            <a href="javascript:void(0)" id="crear-producto"
                                                class="btn btn-outline-success ml-4 mb-4">Nuevo producto</a>
                                            <a href="javascript:void(0)" id="crear-paquete"
                                                class="btn btn-outline-success ml-4 mb-4">Crear Paquete</a>
                                            <a href="javascript:void(0)" id="cerrar-view-crear-paquete"
                                                class="btn btn-outline-danger ml-4 mb-4">Cerrar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row layout-top-spacing" id="form-update-llegada">
                    <div class="container">
                        <div class="container">
                            <div class="row">
                                <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">

                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4 id="title">Actualiza Estado </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">


                                            <form class="m-4" id="edit-estado">
                                                <div class='form-row mb-4'>
                                                <div class="form-group col-md-6" hidden>
                                                        <label for="inputPassword4">ID</label>
                                                        <input type="text" class="form-control" name="id_paquete"
                                                            id="id_paquete" placeholder="id">
                                                    </div>

                                                    <div class='form-group col-md-6'>
                                                        <label for='inputEmail4'>Paquete</label>
                                                        <label class='form-control' name='descripcion_paquete' id='descripcion_paquete2'>
                                                    </div>
                                                    <div class='form-group col-md-6'>
                                                        <label for='inputEmail4'>Numero de Traking</label>
                                                        <label class='form-control' name='numero_traking' id='numero_traking2'>
                                                    </div>
                                                    <div class='form-group col-md-6'>
                                                        <label>Estado</label><br>
                                                        <select class="form-control" name="estado_entrega" id="estado_entrega">
                                                        <option selected disabled>Seleccione</option>
                                                        <option value="RECIBIDO GLENDA EXPRESS USA">RECIBIDO GLENDA EXPRESS USA</option>
                                                        <option value="VUELO ASIGNADO">VUELO ASIGNADO</option>
                                                        <option value="EN TRANSITO A EL SALVADOR">EN TRANSITO A EL SALVADOR</option>
                                                        <option value="EN ADUANAS">EN ADUANAS</option>
                                                        <option value="LIBERADO ADUANA">LIBERADO ADUANA</option>
                                                        <option value="RECIBIDO GLENDA EXPRESS EL SALVADOR">RECIBIDO GLENDA EXPRESS EL SALVADOR</option>
                                                        <option value="EN RUTA LOCAL">EN RUTA LOCAL</option>
                                                        <option value="ENTREGADO">ENTREGADO</option>
                                        
                                                        </select>
                                                    </div>

                                                </div>
                                            </form>
                                            <a href="javascript:void(0)" id="actualizar_estado"
                                                class="btn btn-outline-success ml-4 mb-4">Cambiar Estado</a>
                                            <a href="javascript:void(0)" id="cerrar-estado"
                                                class="btn btn-outline-danger ml-4 mb-4">Cerrar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">Copyright © 2021 <a target="_blank" href="https://designreset.com">DesignReset</a>, All
                        rights reserved.</p>
                </div>
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart">
                            <path
                                d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                            </path>
                        </svg></p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="../../assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../../assets/js/app.js"></script>
    <script src="../../assets/js/proyect/paquetes.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $(document).ready(function () {
            App.init();
        });
    </script>
    <script src="../../assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../../plugins/table/datatable/datatables.js"></script>
    <script>

    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>