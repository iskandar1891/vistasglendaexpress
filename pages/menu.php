<div class="header-container">
    <header class="header navbar navbar-expand-sm">

        <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg
                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-menu">
                <line x1="3" y1="12" x2="21" y2="12"></line>
                <line x1="3" y1="6" x2="21" y2="6"></line>
                <line x1="3" y1="18" x2="21" y2="18"></line>
            </svg></a>

        <div class="nav-logo align-self-center">
            <a class="navbar-brand" href="index.html"><img alt="logo" src="../../assets/img/logo.jpg"> <span
                    class="navbar-brand-name"><!--Titulo Aqui--></span></a>
        </div>

        <ul class="navbar-item topbar-navigation">

            <!--  BEGIN TOPBAR  -->
            <div class="topbar-nav header navbar" role="banner">
                <nav id="topbar">
                    <ul class="navbar-nav theme-brand flex-row  text-center">
                        <li class="nav-item theme-logo">
                            <a href="index.html">
                                <img src="../../assets/img/logo2.svg" class="navbar-logo" alt="logo">
                            </a>
                        </li>
                        <li class="nav-item theme-text">
                            <a href="index.html" class="nav-link"> <!--Titulo Aqui--> </a>
                        </li>
                    </ul>

                    <ul class="list-unstyled menu-categories" id="topAccordion">

                        <li class="menu single-menu active">
                            <a href="#tables" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>

                                    <span>Gestion de clientes</span>
                                </div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-chevron-down">
                                    <polyline points="6 9 12 15 18 9"></polyline>
                                </svg>
                            </a>
                            <ul class="collapse submenu list-unstyled animated fadeInUp" id="tables"
                                data-parent="#topAccordion">
                                <li>
                                    <a href="../clientes/list_clientes.php"> Ver clientes </a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu single-menu active">
                            <a href="#paquetes" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-briefcase">
                                        <rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect>
                                        <path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path>
                                    </svg>

                                    <span>Paquetes</span>
                                </div>
                            </a>
                            <ul class="collapse submenu list-unstyled animated fadeInUp" id="paquetes"
                                data-parent="#topAccordion">
                                <li>
                                    <a href="../paquetes/list_paquetes.php"> Listar Paquetes </a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu single-menu active">
                            <a href="#ordenes" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                    </svg>

                                    <span>Gestion de ordenes</span>
                                </div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-chevron-down">
                                    <polyline points="6 9 12 15 18 9"></polyline>
                                </svg>
                            </a>
                            <ul class="collapse submenu list-unstyled animated fadeInUp" id="ordenes"
                                data-parent="#topAccordion">
                                <li>
                                    <a href="../ordenes/list_ordenes.php"> Ver ordenes </a>
                                </li>
                            </ul>
                        </li>

                        <!--<li class="menu single-menu menu-extras">
                            <a href="#more" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                <div class="">
                                    <span><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg> <span class="d-xl-none">More</span></span>
                                </div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                            </a>
                            <ul class="collapse submenu list-unstyled animated fadeInUp" id="more" data-parent="#topAccordion">
                                <li class="sub-sub-submenu-list">
                                    <a href="#page" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Pages <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
                                    <ul class="collapse list-unstyled sub-submenu animated fadeInUp" id="page" data-parent="#more">
                                        <li>
                                            <a href="pages_helpdesk.html"> Helpdesk </a>
                                        </li>
                                        <li>
                                            <a href="pages_contact_us.html"> Contact Form </a>
                                        </li>
                                        <li>
                                            <a href="pages_faq.html"> FAQ </a>
                                        </li>
                                        <li>
                                            <a href="pages_faq2.html"> FAQ 2 </a>
                                        </li>
                                        <li>
                                            <a href="pages_privacy.html"> Privacy Policy </a>
                                        </li>
                                        <li>
                                            <a href="pages_coming_soon.html" target="_blank"> Coming Soon </a>
                                        </li>
                                        <li>
                                            <a href="user_profile.html"> Profile </a>
                                        </li>
                                        <li>
                                            <a href="user_account_setting.html"> Account Settings </a>
                                        </li>
                                        <li class="sub-sub-submenu-list">
                                            <a href="#pages-error" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Error <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
                                            <ul class="collapse list-unstyled sub-submenu animated fadeInUp" id="pages-error" data-parent="#page"> 
                                                <li>
                                                    <a href="pages_error404.html" target="_blank"> 404 </a>
                                                </li>
                                                <li>
                                                    <a href="pages_error500.html" target="_blank"> 500 </a>
                                                </li>
                                                <li>
                                                    <a href="pages_error503.html" target="_blank"> 503 </a>
                                                </li>
                                                <li>
                                                    <a href="pages_maintenence.html" target="_blank"> Maintanence </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="sub-sub-submenu-list">
                                            <a href="#user-login" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Login <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
                                            <ul class="collapse list-unstyled sub-submenu animated fadeInUp" id="user-login" data-parent="#page"> 
                                                <li>
                                                    <a href="auth_login.html" target="_blank"> Login </a>
                                                </li>
                                                <li>
                                                    <a href="auth_login_boxed.html" target="_blank"> Login Boxed </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="sub-sub-submenu-list">
                                            <a href="#user-register" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Register <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
                                            <ul class="collapse list-unstyled sub-submenu animated fadeInUp" id="user-register" data-parent="#page"> 
                                                <li>
                                                    <a target="_blank" href="auth_register.html"> Register </a>
                                                </li>
                                                <li>
                                                    <a target="_blank" href="auth_register_boxed.html"> Register Boxed </a>
                                                </li>
                                            </ul>
                                        </li>
            
                                        <li class="sub-sub-submenu-list">
                                            <a href="#user-passRecovery" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Password Recovery <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
                                            <ul class="collapse list-unstyled sub-submenu animated fadeInUp" id="user-passRecovery" data-parent="#page"> 
                                                <li>
                                                    <a target="_blank" href="auth_pass_recovery.html"> Recover ID </a>
                                                </li>
                                                <li>
                                                    <a target="_blank" href="auth_pass_recovery_boxed.html"> Recover ID Boxed </a>
                                                </li>
                                            </ul>
                                        </li>
            
                                        <li class="sub-sub-submenu-list">
                                            <a href="#user-lockscreen" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Lockscreen <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
                                            <ul class="collapse list-unstyled sub-submenu animated fadeInUp" id="user-lockscreen" data-parent="#page"> 
                                                <li>
                                                    <a href="auth_lockscreen.html" target="_blank"> Unlock </a>
                                                </li>
                                                <li>
                                                    <a href="auth_lockscreen_boxed.html" target="_blank"> Unlock Boxed </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="dragndrop_dragula.html"> Drag and Drop</a>
                                </li>
                                <li>
                                    <a href="widgets.html"> Widgets </a>
                                </li>
                                <li>
                                    <a href="map_jvector.html"> Vector Maps</a>
                                </li>
                                <li>
                                    <a href="charts_apex.html"> Charts </a>
                                </li>
                                <li>
                                    <a href="fonticons.html"> Font Icons </a>
                                </li>
                                <li class="sub-sub-submenu-list">
                                    <a href="#starter-kit" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Starter Kit <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
                                    <ul class="collapse list-unstyled sub-submenu animated fadeInUp" id="starter-kit" data-parent="#more">
                                        <li>
                                            <a href="starter_kit_blank_page.html"> Blank Page </a>
                                        </li>
                                        <li>
                                            <a href="starter_kit_boxed.html"> Boxed </a>
                                        </li>
                                        <li>
                                            <a href="starter_kit_breadcrumb.html"> Breadcrumb </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a target="_blank" href="../../documentation/index.html"> Documentation </a>
                                </li>
                            </ul>
                        </li>aqui-->
                    </ul>
                </nav>
            </div>
            <!--  END TOPBAR  -->

        </ul>

        <ul class="navbar-item flex-row ml-auto">
            <li class="nav-item align-self-center search-animated">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-search toggle-search">
                    <circle cx="11" cy="11" r="8"></circle>
                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                </svg>
                <form class="form-inline search-full form-inline search" role="search">
                    <div class="search-bar">
                        <input type="text" class="form-control search-form-control  ml-lg-auto" placeholder="Search...">
                    </div>
                </form>
            </li>
        </ul>

        <ul class="navbar-item flex-row nav-dropdowns">
            <li class="nav-item dropdown user-profile-dropdown order-lg-0 order-1">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="user-profile-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media">
                        <img src="../../assets/img/profile-7.jpeg" class="img-fluid" alt="admin-profile">
                    </div>
                </a>
                <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                    <div class="user-profile-section">
                        <div class="media mx-auto">
                            <div class="media-body">
                                <h5>Shaun Park</h5>
                                <p>Project Leader</p>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-item">
                        <a href="user_profile.html">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-user">
                                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                <circle cx="12" cy="7" r="4"></circle>
                            </svg> <span>Profile</span>
                        </a>
                    </div>
                    <div class="dropdown-item">
                        <a href="auth_login.html">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-log-out">
                                <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                <polyline points="16 17 21 12 16 7"></polyline>
                                <line x1="21" y1="12" x2="9" y2="12"></line>
                            </svg> <span>Log Out</span>
                        </a>
                    </div>
                </div>

            </li>
        </ul>
    </header>
</div>
<style>

.navbar .nav-logo a.navbar-brand img {
    width: 150px!important;
    height: 50px!important;
}
</style>